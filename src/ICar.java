
public interface ICar {
	String getMake(); // this is always public . This was the first thing we made
	String getModel();
	int getYear();
	int getMileage();
}
